import React from 'react'
import PropTypes from 'prop-types'
import { Paper, Button, Box, Grid } from '@material-ui/core'
import DeliveryType from './components/DeliveryType'
import Payment from './components/Payment'
import DeliveryDestination from './components/DeliveryDestination'

const Order = ({ formValues, handleChange, handleSubmit }) => {
  return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit}>
      <Box m={2}>
        <Paper>
          <Box p={2}>
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="center">
              <Grid item>
                <DeliveryType
                  selected={formValues.deliveryType}
                  handleChange={handleChange}
                />
              </Grid>
              <Grid item>
                <Payment
                  selected={formValues.paymentType}
                  handleChange={handleChange}
                />
              </Grid>
              <Grid item>
                <DeliveryDestination
                  formValues={formValues}
                  handleChange={handleChange}
                />
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>
      <Button type="submit" variant="contained" color="primary">
        Submit
      </Button>
    </form>
  )
}

Order.propTypes = {
  formValues: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

export default Order
