import React, { Component } from 'react'
import Order from './Order'
import route from '../../static/route'

class OrderContainer extends Component {
  state = {
    deliveryType: '',
    paymentType: '',
    name: '',
    street: '',
    city: '',
    postalCode: '',
  }

  handleChange = event => {
    const name = event.target.name
    this.setState({
      [name]: event.target.value,
    })
  }

  handleSubmit = () => {
    alert(JSON.stringify(this.state, null, 2))
    this.props.history.push(route.CONFIRMATION)
  }

  render() {
    return (
      <Order
        formValues={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
      />
    )
  }
}

OrderContainer.propTypes = {}

export default OrderContainer
