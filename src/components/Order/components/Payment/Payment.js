import React from 'react'
import {
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  Typography,
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { paymentType } from '../../static/paymentType'

const Payment = ({ selected, handleChange }) => {
  return (
    <>
      <Typography variant="h6">2. Payment Type</Typography>
      <FormControl component="fieldset">
        <RadioGroup
          aria-label="position"
          name="paymentType"
          value={selected}
          onChange={handleChange}
          row>
          <FormControlLabel
            value={paymentType.TRANSFER}
            control={<Radio color="primary" />}
            label={paymentType.TRANSFER}
          />
          <FormControlLabel
            value={paymentType.PAYU}
            control={<Radio color="primary" />}
            label={paymentType.PAYU}
          />
        </RadioGroup>
      </FormControl>
    </>
  )
}

Payment.propTypes = {
  selected: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default Payment
