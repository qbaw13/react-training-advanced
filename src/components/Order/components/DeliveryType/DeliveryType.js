import React from 'react'
import {
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  Typography,
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { deliveryType } from '../../static/deliveryType'

const DeliveryType = ({ selected, handleChange }) => {
  return (
    <>
      <Typography variant="h6">1. Delivery Type</Typography>
      <FormControl component="fieldset">
        <RadioGroup
          aria-label="position"
          name="deliveryType"
          value={selected}
          onChange={handleChange}
          row>
          <FormControlLabel
            value={deliveryType.COURIER}
            control={<Radio color="primary" />}
            label={deliveryType.COURIER}
          />
          <FormControlLabel
            value={deliveryType.POSTAL_DELIVERY}
            control={<Radio color="primary" />}
            label={deliveryType.POSTAL_DELIVERY}
          />
        </RadioGroup>
      </FormControl>
    </>
  )
}

DeliveryType.propTypes = {
  selected: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default DeliveryType
