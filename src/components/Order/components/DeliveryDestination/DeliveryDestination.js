import React from 'react'
import { Typography } from '@material-ui/core'
import PropTypes from 'prop-types'
import Address from './Address'

const DeliveryDestination = ({ handleChange, formValues }) => {
  const { name, street, city, postalCode } = formValues

  return (
    <>
      <Typography variant="h6">3. Delivery destination</Typography>
      <Address
        name={name}
        street={street}
        city={city}
        postalCode={postalCode}
        handleChange={handleChange}
      />
    </>
  )
}

DeliveryDestination.propTypes = {
  formValues: PropTypes.shape({
    name: PropTypes.string,
    street: PropTypes.string,
    city: PropTypes.string,
    postalCode: PropTypes.string,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default DeliveryDestination
