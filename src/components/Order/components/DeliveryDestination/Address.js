import React from 'react'
import { TextField } from '@material-ui/core'
import PropTypes from 'prop-types'

const Address = ({ name, street, city, postalCode, handleChange }) => {
  return (
    <>
      <TextField
        id="name"
        name="name"
        label="Name"
        value={name}
        onChange={handleChange}
        fullWidth
      />
      <TextField
        id="street"
        name="street"
        label="Street"
        value={street}
        onChange={handleChange}
        fullWidth
      />
      <TextField
        id="city"
        name="city"
        label="City"
        value={city}
        onChange={handleChange}
        fullWidth
      />
      <TextField
        id="postal-code"
        name="postalCode"
        label="Postal code"
        value={postalCode}
        onChange={handleChange}
        fullWidth
      />
    </>
  )
}

Address.propTypes = {
  name: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  postalCode: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default Address
