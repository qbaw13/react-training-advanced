import React from 'react'
import { AppBar as MuiAppBar, Toolbar, Typography } from '@material-ui/core'
import { Link } from 'react-router-dom'
import route from '../static/route'

const AppBar = () => (
  <MuiAppBar position="static">
    <Toolbar>
      <Typography variant="h6">
        <Link
          to={route.ORDER}
          style={{ textDecoration: 'none', color: 'unset' }}>
          Shop
        </Link>
      </Typography>
    </Toolbar>
  </MuiAppBar>
)

export default AppBar
