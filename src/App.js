import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Confirmation from './components/Confirmation'
import Order from './components/Order'
import './App.css'
import { Container, Box } from '@material-ui/core'
import AppBar from './layout/AppBar'
import route from './static/route'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppBar />
        <Container>
          <Box my={2}>
            <Switch>
              <Route exact path={route.ORDER} component={Order} />
              <Route exact path={route.CONFIRMATION} component={Confirmation} />
            </Switch>
          </Box>
        </Container>
      </div>
    </BrowserRouter>
  )
}

export default App
